Given 'user access to main page' do

    @app.home_page.load
    sleep(5)
end

When 'user open login' do
    @app.home_page.button_login_regis.click
    sleep(5)
end

When 'user fill login with {string}' do |parameter|

    if parameter.eql? "email null"
        @app.login_page.input_email_login.set ''
    elsif parameter.eql? "format email aaa"
        @app.login_page.input_email_login.set 'mohs.anifff'
    else
        @app.login_page.input_email_login.set 'mohs.anifff@gmail.com'
    end
    sleep(5)
    if parameter.eql? "password null"
        @app.login_page.input_password_login.set ''
    else
        @app.login_page.input_password_login.set 'arief'
    end
    sleep(5)
    @app.login_page.button_login.click
    sleep(5)
end

Then 'user success login' do
    expect(@app.home_page.has_name_profile?).to be true
end

Then 'user invalid login email null' do
    expect(@app.login_page.has_error_state_email?).to be true
end

Then 'user invalid login password null' do
    expect(@app.login_page.has_error_state_password?).to be true
end

Then 'user got error message' do
    expect(@app.login_page.has_error_message?).to be true
    @app.login_page.error_message.any? { |error| error.text.include? '*Wajib Diisi'}
end

Then 'user invalid login with format email' do
    expect(@app.login_page.has_error_format_email?).to be true
end

